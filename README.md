# Module: Organizations SCPs Mandatory Guardrails

## Overview

This Module contains most of the Guardrails described in the AWS Control Tower Guardrail Reference: <https://docs.aws.amazon.com/controltower/latest/userguide/guardrails-reference.html> (as of 28.04.2021).

## Details

### Mandatory Guardrails

- Disallow Changes to Encryption Configuration for AWS Control Tower Created S3 Buckets in Log Archive (GRCTAUDITBUCKETENCRYPTIONCHANGESPROHIBITED)
- Disallow Changes to Logging Configuration for AWS Control Tower Created S3 Buckets in Log Archive (GRCTAUDITBUCKETLOGGINGCONFIGURATIONCHANGESPROHIBITED)
- Disallow Changes to Bucket Policy for AWS Control Tower Created S3 Buckets in Log Archive (GRCTAUDITBUCKETPOLICYCHANGESPROHIBITED)
- Disallow Changes to Lifecycle Configuration for AWS Control Tower Created S3 Buckets in Log Archive (GRCTAUDITBUCKETLIFECYCLECONFIGURATIONCHANGESPROHIBITED)
- Disallow Changes to CloudWatch Logs Log Groups (GRLOGGROUPPOLICY)
- Disallow Deletion of AWS Config Aggregation Authorization  (GRCONFIGAGGREGATIONAUTHORIZATIONPOLICY)
- Disallow Deletion of Log Archive (GRAUDITBUCKETDELETIONPROHIBITED)
- Disallow Configuration Changes to CloudTrail (GRCLOUDTRAILENABLED)
- Integrate CloudTrail Events with CloudWatch Logs (GRCLOUDTRAILENABLED)
- Enable CloudTrail in All Available Regions (GRCLOUDTRAILENABLED)
- Enable Integrity Validation for CloudTrail Log File (GRCLOUDTRAILENABLED)
- Disallow Changes to CloudWatch Set Up by AWS Control Tower (GRCLOUDWATCHEVENTPOLICY)
- Disallow Changes to AWS Config Aggregation Set Up by AWS Control Tower (GRCONFIGRULETAGSPOLICY)
- Disallow Configuration Changes to AWS Config (GRCONFIGENABLED)
- Enable AWS Config in All Available Regions (GRCONFIGENABLED)
- Disallow Changes to AWS Config Rules Set Up by AWS Control Tower (GRCONFIGRULEPOLICY)
- Disallow Changes to IAM Roles Set Up by AWS Control Tower (GRIAMROLEPOLICY)
- Disallow Changes to Lambda Functions Set Up by AWS Control Tower (GRLAMBDAFUNCTIONPOLICY)
- Disallow Changes to Amazon SNS Set Up by AWS Control Tower  (GRSNSTOPICPOLICY)
- Disallow Changes to Amazon SNS Subscriptions Set Up by AWS Control Tower (GRSNSSUBSCRIPTIONPOLICY)

Currently not included (as they're Config Rules):

- Disallow Public Read Access to Log Archive
- Disallow Public Write Access to Log Archive

### Strongly Recommended Guardrails

- Disallow Creation of Access Keys for the Root User (GRRESTRICTROOTUSERACCESSKEYS)
- Disallow Actions as a Root User (GRRESTRICTROOTUSER)

Currently not included (as they're Config Rules):

- Enable Encryption for Amazon EBS Volumes Attached to Amazon EC2 Instances
- Disallow Internet Connection Through RDP
- Disallow Internet Connection Through SSH
- Enable MFA for the Root User
- Disallow Public Read Access to Amazon S3 Buckets
- Disallow Public Write Access to Amazon S3 Buckets
- Disallow Amazon EBS Volumes That Are Unattached to An Amazon EC2 Instance
- Disallow Amazon EC2Instance Types That Are Not Amazon EBS-Optimized
- Disallow Public Access to Amazon RDS Database Instances
- Disallow Public Access to Amazon RDS Database Snapshots
- Disallow Amazon RDS Database Instances That Are Not Storage Encrypted

### Additional SCPs:

- Deny All Services Outside the EU (DenyAllOutsideEU)

## Usage

````
module "organizations_scp_mandatory_guardrails" {
  source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-organizations-guardrails-spcs.git?ref=0.0.1"

  organizations_access_role_name = var.organizations_access_role_name
  locked_orchestration_suffix = "tflz"
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_organizations_policy.scp_account_mandatory_1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy) | resource |
| [aws_organizations_policy.scp_account_mandatory_2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy) | resource |
| [aws_organizations_policy.scp_account_strongly_recommended](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy) | resource |
| [aws_organizations_policy.scp_eu_only](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy) | resource |
| [aws_organizations_policy_attachment.scp_account_mandatory_1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy_attachment) | resource |
| [aws_organizations_policy_attachment.scp_account_mandatory_2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy_attachment) | resource |
| [aws_organizations_policy_attachment.scp_account_strongly_recommended](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy_attachment) | resource |
| [aws_organizations_policy_attachment.scp_eu_only](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy_attachment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_apply_to_ous_or_accounts"></a> [apply\_to\_ous\_or\_accounts](#input\_apply\_to\_ous\_or\_accounts) | A list of AWS Organization OU IDs or AWS Account IDs that should have the Policies applied (can be empty). | `list(string)` | `[]` | no |
| <a name="input_locked_orchestration_suffix"></a> [locked\_orchestration\_suffix](#input\_locked\_orchestration\_suffix) | The suffix which will be used to mark resources managed by this landingzone | `any` | n/a | yes |
| <a name="input_organizations_access_role_name"></a> [organizations\_access\_role\_name](#input\_organizations\_access\_role\_name) | The rolename of the admin access role in the accounts. Defaults to OrganizationAccountAccessRole | `string` | `"OrganizationAccountAccessRole"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags for all AWS Resources | `any` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
