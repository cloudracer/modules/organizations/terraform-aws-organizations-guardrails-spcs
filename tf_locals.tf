locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-organizations-guardrails-spcs"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-organizations-guardrails-spcs"
  }
  tags = merge(local.tags_module, var.tags)
}
