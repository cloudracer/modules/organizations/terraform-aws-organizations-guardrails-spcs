resource "aws_organizations_policy" "scp_account_mandatory_1" {
  name = "scp_account_mandatory_part_1"

  description = "Contains all Mandatory SCP´s (Part I) for every Account."

  content = templatefile("${path.module}/templates/scp_any_account_mandatory_1.tpl", {
    org_role_arn                = "arn:aws:iam::*:role/${var.organizations_access_role_name}"
    locked_orchestration_suffix = var.locked_orchestration_suffix
  })

  tags = local.tags
}

resource "aws_organizations_policy_attachment" "scp_account_mandatory_1" {
  for_each = toset(var.apply_to_ous_or_accounts)

  policy_id = aws_organizations_policy.scp_account_mandatory_1.id
  target_id = each.value
}

resource "aws_organizations_policy" "scp_account_mandatory_2" {
  name = "scp_account_mandatory_part_2"

  description = "Contains all Mandatory SCP´s (Part II) for every Account."

  content = templatefile("${path.module}/templates/scp_any_account_mandatory_2.tpl", {
    org_role_arn                = "arn:aws:iam::*:role/${var.organizations_access_role_name}"
    locked_orchestration_suffix = var.locked_orchestration_suffix
  })

  tags = local.tags
}

resource "aws_organizations_policy_attachment" "scp_account_mandatory_2" {
  for_each = toset(var.apply_to_ous_or_accounts)

  policy_id = aws_organizations_policy.scp_account_mandatory_2.id
  target_id = each.value
}

resource "aws_organizations_policy" "scp_account_strongly_recommended" {
  name = "scp_account_strongly_recommended"

  description = "Contains all strongly recommended SCP´s for every Account."

  content = templatefile("${path.module}/templates/scp_any_account_strongly_recommended.json", {
    org_role_arn                = "arn:aws:iam::*:role/${var.organizations_access_role_name}"
    locked_orchestration_suffix = var.locked_orchestration_suffix
  })

  tags = local.tags
}

resource "aws_organizations_policy_attachment" "scp_account_strongly_recommended" {
  for_each = toset(var.apply_to_ous_or_accounts)

  policy_id = aws_organizations_policy.scp_account_strongly_recommended.id
  target_id = each.value
}

resource "aws_organizations_policy" "scp_eu_only" {
  name = "scp_eu_only"

  description = "Restricts access to use EU regions only."

  content = templatefile("${path.module}/templates/scp_eu_only.json", {
    org_role_arn                = "arn:aws:iam::*:role/${var.organizations_access_role_name}"
    locked_orchestration_suffix = var.locked_orchestration_suffix
  })

  tags = local.tags
}

resource "aws_organizations_policy_attachment" "scp_eu_only" {
  for_each = toset(var.apply_to_ous_or_accounts)

  policy_id = aws_organizations_policy.scp_eu_only.id
  target_id = each.value
}
