{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "DisallowChangesToEncryptionConfigByTflz",
      "Effect": "Deny",
      "Action": [
        "s3:PutEncryptionConfiguration"
      ],
      "Resource": ["arn:aws:s3:::*${locked_orchestration_suffix}*"],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN":"${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowChangesToS3LoggingConfigByTflz",
      "Effect": "Deny",
      "Action": [
        "s3:PutBucketLogging"
      ],
      "Resource": ["arn:aws:s3:::*${locked_orchestration_suffix}*"],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN":"${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowChangesToBucketPoliciesByTflz",
      "Effect": "Deny",
      "Action": [
        "s3:PutBucketPolicy",
        "s3:DeleteBucketPolicy"
      ],
      "Resource": ["arn:aws:s3:::*${locked_orchestration_suffix}*"],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN":"${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowChangesToLifecyleConfigForS3ByTflz",
      "Effect": "Deny",
      "Action": [
        "s3:PutLifecycleConfiguration"
      ],
      "Resource": ["arn:aws:s3:::*${locked_orchestration_suffix}*"],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN":"${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowCloudWatchLogsChanges",
      "Effect": "Deny",
      "Action": [
        "logs:DeleteLogGroup",
        "logs:PutRetentionPolicy"
      ],
      "Resource": [
        "arn:aws:logs:*:*:log-group:*${locked_orchestration_suffix}*"
      ],
      "Condition": {
        "StringNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowDeletionOfAWSConfigAggregators",
      "Effect": "Deny",
      "Action": [
        "config:DeleteAggregationAuthorization"
      ],
      "Resource": [
        "arn:aws:config:*:*:aggregation-authorization*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        },
        "StringLike": {
          "aws:ResourceTag/aws-landingzone": "managed-by-aws-tf-landingzone"
        }
      }
    },
    {
      "Sid": "DisallowDeletionOfLogArchive",
      "Effect": "Deny",
      "Action": [
        "s3:DeleteBucket"
        ],
      "Resource": [
        "arn:aws:s3:::*${locked_orchestration_suffix}*"
        ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN":"${org_role_arn}"
          }
      }
    },
    {
      "Sid": "DisallowCloudTrailConfigChanges",
      "Effect": "Deny",
      "Action": [
        "cloudtrail:DeleteTrail",
        "cloudtrail:PutEventSelectors",
        "cloudtrail:StopLogging",
        "cloudtrail:UpdateTrail"
      ],
      "Resource": ["arn:aws:cloudtrail:*:*:trail/*${locked_orchestration_suffix}*"],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN":"${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowChangesToCloudWatchByTflz",
      "Effect": "Deny",
      "Action": [
        "events:PutRule",
        "events:PutTargets",
        "events:RemoveTargets",
        "events:DisableRule",
        "events:DeleteRule"
      ],
      "Resource": [
        "arn:aws:events:*:*:rule/*${locked_orchestration_suffix}*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowAwsConfigTagging",
      "Effect": "Deny",
      "Action": [
        "config:TagResource",
        "config:UntagResource"
      ],
      "Resource": [
        "*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        },
        "ForAllValues:StringEquals": {
          "aws:TagKeys": "aws-landingzone"
        }
      }
    }
  ]
}
