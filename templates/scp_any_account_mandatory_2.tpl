{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "DisallowAwsConfigChanges",
      "Effect": "Deny",
      "Action": [
        "config:DeleteConfigurationRecorder",
        "config:DeleteDeliveryChannel",
        "config:DeleteRetentionConfiguration",
        "config:PutConfigurationRecorder",
        "config:PutDeliveryChannel",
        "config:PutRetentionConfiguration",
        "config:StopConfigurationRecorder"
      ],
      "Resource": [
        "*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowAwsConfigRulesByTflz",
      "Effect": "Deny",
      "Action": [
        "config:PutConfigRule",
        "config:DeleteConfigRule",
        "config:DeleteEvaluationResults",
        "config:DeleteConfigurationAggregator",
        "config:PutConfigurationAggregator"
      ],
      "Resource": [
        "*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        },
        "StringEquals": {
          "aws:ResourceTag/aws-landingzone": "managed-by-aws-tf-landingzone"
        }
      }
    },
    {
      "Sid": "DisallowChangesToIamRolesByTflz",
      "Effect": "Deny",
      "Action": [
        "iam:AttachRolePolicy",
        "iam:CreateRole",
        "iam:DeleteRole",
        "iam:DeleteRolePermissionsBoundary",
        "iam:DeleteRolePolicy",
        "iam:DetachRolePolicy",
        "iam:PutRolePermissionsBoundary",
        "iam:PutRolePolicy",
        "iam:UpdateAssumeRolePolicy",
        "iam:UpdateRole",
        "iam:UpdateRoleDescription"
      ],
      "Resource": [
        "arn:aws:iam::*:role/${locked_orchestration_suffix}*",
        "arn:aws:iam::*:role/*${locked_orchestration_suffix}*",
        "arn:aws:iam::*:role/${org_role_arn}"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowChangesToLambdaFunctionsByTflz",
      "Effect": "Deny",
      "Action": [
        "lambda:AddPermission",
        "lambda:CreateEventSourceMapping",
        "lambda:CreateFunction",
        "lambda:DeleteEventSourceMapping",
        "lambda:DeleteFunction",
        "lambda:DeleteFunctionConcurrency",
        "lambda:PutFunctionConcurrency",
        "lambda:RemovePermission",
        "lambda:UpdateEventSourceMapping",
        "lambda:UpdateFunctionCode",
        "lambda:UpdateFunctionConfiguration"
      ],
      "Resource": [
        "arn:aws:lambda:*:*:function:*${locked_orchestration_suffix}*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowChangesToSnsByTflz",
      "Effect": "Deny",
      "Action": [
        "sns:AddPermission",
        "sns:CreateTopic",
        "sns:DeleteTopic",
        "sns:RemovePermission",
        "sns:SetTopicAttributes"
      ],
      "Resource": [
        "arn:aws:sns:*:*:*${locked_orchestration_suffix}*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        }
      }
    },
    {
      "Sid": "DisallowChangesToSNSSubscriptionsByTflz",
      "Effect": "Deny",
      "Action": [
        "sns:Subscribe",
        "sns:Unsubscribe"
      ],
      "Resource": [
        "arn:aws:sns:*:*:*${locked_orchestration_suffix}*"
      ],
      "Condition": {
        "ArnNotLike": {
          "aws:PrincipalARN": "${org_role_arn}"
        }
      }
    }
  ]
}
