variable "organizations_access_role_name" {
  description = "The rolename of the admin access role in the accounts. Defaults to OrganizationAccountAccessRole"
  default     = "OrganizationAccountAccessRole"
}

variable "locked_orchestration_suffix" {
  description = "The suffix which will be used to mark resources managed by this landingzone"
}

variable "apply_to_ous_or_accounts" {
  type        = list(string)
  description = "A list of AWS Organization OU IDs or AWS Account IDs that should have the Policies applied (can be empty)."
  default     = []
}

variable "tags" {
  description = "Tags for all AWS Resources"
}
